Name:          kylin-scanner
Version:       1.0.4
Release:       2
Summary:       Kylin Scanner is an interface-friendly scanning software developed with Qt5.
License:       BSL-1.0 and Libpng and zlib and GPL-2.0-or-later
URL:           https://github.com/UbuntuKylin/kylin-scanner
Source0:       %{name}-%{version}.tar.gz
Patch01:       0001-fix-compile-error-of-kylin-scanner.patch
Patch02:       0002-modify-version-of-kylin-scanner-is-null.patch 
Patch03:       0001-kylin-scanner-fix-some-build-warnings-and-code-probl.patch

BuildRequires: qt5-qtsvg-devel
BuildRequires: glib2-devel
BuildRequires: libX11-devel
BuildRequires: gsettings-qt-devel
BuildRequires: sane-backends-devel
BuildRequires: opencv
BuildRequires: stb-devel
BuildRequires: giflib-devel               
BuildRequires: libpng-devel
BuildRequires: freeimage-devel
BuildRequires: tesseract-devel
BuildRequires: leptonica-devel leptonica
BuildRequires: qt5-qtbase-devel
BuildRequires: qt5-qttools-devel qt5-linguist
BuildRequires: kf5-kwindowsystem-devel
BuildRequires: qt5-qtx11extras-devel
BuildRequires: ukui-interface
BuildRequires: polkit-devel

Requires: tesseract


%description
Scanning utility based on SANE
Kylin-scanner can scan according to the resolution, size and color
mode of the scanning device.
And it provides post-processing features for scanned images, such as
clipping, rotation, one-click beautification, intelligent correction
and text recognition, etc.


%prep
%autosetup -n %{name}-%{version} -p1

%build
%{qmake_qt5}
%{make_build}

%install
%{make_install} INSTALL_ROOT=%{buildroot}

%files
%{_bindir}/kylin-scanner
%{_datadir}/applications/kylin-scanner.desktop
%{_datadir}/kylin-scanner/translations/*.qm
%{_datadir}/pixmaps/scanner.png
%{_datadir}/kylin-user-guide/data/guide/kylin-scanner

%changelog
* Tue Apr 23 2024 houhongxun <houhongxun@kylinos.cn> - 1.0.4-2
- fix some build warnings and code problems

* Thu May 25 2023 peijiankang <peijiankang@kylinos.cn> - 1.0.4-1
- update version to 1.0.4

* Mon Feb 06 2023 peijiankang <peijiankang@kylinos.cn> - 1.0.3-5
- add build debuginfo and debugsource

* Fri Jun 24 2022 peijiankang <peijiankang@kylinos.cn> - 1.0.3-4
- update upstream version

* Mon May 30 2022 peijiankang <peijiankang@kylinos.cn> - 1.0.3-3
- modify version of kylin-scanner is null  

* Thu May 26 2022 peijiankang <peijiankang@kylinos.cn> - 1.0.3-2 
- remove {%if 0 and %endif}                                       

* Fri May 20 2022 peijiankang <peijiankang@kylinos.cn> - 1.0.3-1
- Init kylin-scanner package for openEuler
